package net.avcompris.status.api;

public interface ServiceStatusHistory {

	String getServiceId();

	String getEndpoint();

	Check[] getChecks();

	int getStart();

	int getTotal();
}
