package net.avcompris.status.api;

import javax.annotation.Nullable;

public interface InlineService {

	String getEndpoint();

	@Nullable
	Long getTimeOutMs();

	@Nullable
	Expect getExpect();

	InlineService setEndpoint(String endpoint);

	InlineService setTimeOutMs(@Nullable Long timeOutMs);

	InlineService setExpect(@Nullable Expect expect);

	interface Expect extends StatusConfig.Expect {

		Expect setStatusCode(@Nullable Integer statusCode);
	}
}
