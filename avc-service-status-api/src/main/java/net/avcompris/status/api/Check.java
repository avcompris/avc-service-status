package net.avcompris.status.api;

import javax.annotation.Nullable;

import net.avcompris.commons3.types.DateTimeHolder;
import net.avcompris.status.query.CheckStatus;

public interface Check {

	String getId();

	DateTimeHolder getStartedAt();

	@Nullable
	DateTimeHolder getEndedAt();

	@Nullable
	Integer getElapsedMs();

	boolean isSuccess();

	CheckStatus getStatus();

	@Nullable
	Integer getStatusCode();

	@Nullable
	String getErrorMessage();

	TriggerType getTriggerType();
}
