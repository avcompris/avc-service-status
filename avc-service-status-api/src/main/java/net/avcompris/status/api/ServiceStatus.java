package net.avcompris.status.api;

public interface ServiceStatus {

	String getServiceId();

	String getEndpoint();

	Check getCheck();
}
