package net.avcompris.status.api;

public interface ServicesStatusHistory {

	ServiceStatusHistory[] getItems();
}
