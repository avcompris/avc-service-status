package net.avcompris.status.api;

import static net.avcompris.status.api.Permission.ANY;
import static net.avcompris.status.api.Permission.GET_ANY_STATUS;
import static net.avcompris.status.api.Permission.QUERY_ALL_STATUS;

import net.avcompris.commons3.api.exception.ServiceException;

public interface StatusService {

	@Permissions(QUERY_ALL_STATUS)
	ServicesStatus getServicesLiveStatus(String correlationId) throws ServiceException;

	@Permissions(GET_ANY_STATUS)
	ServiceStatus getServiceLiveStatus(String correlationId, //
			String serviceId //
	) throws ServiceException;

	@Permissions(GET_ANY_STATUS)
	ServiceStatus getInlineServiceLiveStatus(String correlationId, //
			String serviceId, //
			InlineService inlineService //
	) throws ServiceException;

	@Permissions(QUERY_ALL_STATUS)
	ServicesStatusHistory getServicesStatusHistory(String correlationId) throws ServiceException;

	@Permissions(GET_ANY_STATUS)
	ServiceStatusHistory getServiceStatusHistory(String correlationId, //
			String serviceId //
	) throws ServiceException;

	@Permissions(ANY)
	StatusConfig getStatusConfig(String correlationId) throws ServiceException;
}
