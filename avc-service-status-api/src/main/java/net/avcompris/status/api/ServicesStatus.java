package net.avcompris.status.api;

import net.avcompris.commons3.types.DateTimeHolder;

public interface ServicesStatus {

	DateTimeHolder getCheckStartedAt();

	DateTimeHolder getCheckEndedAt();

	ServiceStatus[] getItems();

	TriggerType getTriggerType();
}
