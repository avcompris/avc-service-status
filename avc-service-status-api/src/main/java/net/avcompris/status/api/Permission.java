package net.avcompris.status.api;

import javax.annotation.Nullable;

import net.avcompris.commons3.api.EnumPermission;

public enum Permission implements EnumPermission {

	// ======== ANONYMOUS ========

	ANY,

	// ======== ADMIN ========

	SUPERADMIN,

	QUERY_ALL_STATUS, GET_ANY_STATUS,

	WORKERS, PURGE_CORRELATION_IDS,

	// ======== ROUTING ========

	ROUTE;

	@Override
	public boolean isSuperadminPermission() {

		return this == SUPERADMIN;
	}

	@Override
	public boolean isAnyUserPermission() {

		return this == ANY;
	}

	@Override
	@Nullable
	public String getExpression() {

		return null;
	}
}
