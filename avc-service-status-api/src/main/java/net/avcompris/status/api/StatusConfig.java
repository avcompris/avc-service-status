package net.avcompris.status.api;

import javax.annotation.Nullable;

public interface StatusConfig {

	ServiceConfig[] getServices();

	interface ServiceConfig {

		String getId();

		String getEndpoint();

		String[] getLabels();

		@Nullable
		Integer getTimeOutMs();

		@Nullable
		Integer getEveryMs();

		@Nullable
		Expect getExpect();
	}

	interface Expect {

		@Nullable
		Integer getStatusCode();
	}
}
