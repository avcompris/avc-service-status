package net.avcompris.status.api;

public enum TriggerType {

	MANUAL, CRON, UNKNOWN,
}
