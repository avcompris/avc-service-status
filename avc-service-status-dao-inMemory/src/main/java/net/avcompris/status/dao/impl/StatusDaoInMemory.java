package net.avcompris.status.dao.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Maps.newHashMap;
import static net.avcompris.commons3.databeans.DataBeans.instantiate;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.Map;

import javax.annotation.Nullable;

import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Multimap;
import com.google.common.collect.TreeMultimap;

import net.avcompris.commons3.dao.impl.AbstractDao;
import net.avcompris.commons3.utils.Clock;
import net.avcompris.status.dao.CheckDto;
import net.avcompris.status.dao.EndpointDto;
import net.avcompris.status.dao.ServicesStatusHistoryDto;
import net.avcompris.status.dao.StatusDao;
import net.avcompris.status.query.CheckStatus;

@Component
public final class StatusDaoInMemory extends AbstractDao implements StatusDao {

	private final Multimap<String, CheckDto> checksByEndpoints = TreeMultimap.create( //
			new Comparator<String>() {
				@Override
				public int compare(final String s1, final String s2) {
					return s1.compareTo(s2);
				}
			}, //
			new Comparator<CheckDto>() {
				@Override
				public int compare(final CheckDto c1, final CheckDto c2) {
					return -c1.getStartedAt().compareTo(c2.getStartedAt());
				}
			});

	private final Map<String, MutableCheckDto> checksByIds = newHashMap();

	private final Multimap<String, String> errorMessages = TreeMultimap.create();

	@Autowired
	public StatusDaoInMemory(final Clock clock) {

		super(clock);
	}

	@Override
	public ServicesStatusHistoryDto getServicesCachedStatus(final EndpointDto... endpoints)
			throws SQLException, IOException {

		checkNotNull(endpoints, "endpoints");

		final MutableServicesStatusHistoryDto servicesStatus = instantiate(MutableServicesStatusHistoryDto.class);

		for (final EndpointDto e : endpoints) {

			final String serviceId = e.getServiceId();
			final String endpoint = e.getEndpoint();

			final MutableServiceStatusHistoryDto serviceStatusHistory = instantiate(
					MutableServiceStatusHistoryDto.class) //
							.setServiceId(serviceId) //
							.setEndpoint(endpoint) //
							.setStart(0) //
							.setTotal(checksByEndpoints.get(endpoint).size());

			for (final CheckDto check : checksByEndpoints.get(endpoint)) {

				serviceStatusHistory.addToChecks(check);
			}

			servicesStatus.addToItems(serviceStatusHistory);
		}

		return servicesStatus;
	}

	@Override
	public String initCheck( //
			final String serviceId, //
			final String endpoint //
	) throws SQLException, IOException {

		checkNotNull(serviceId, "serviceId");
		checkNotNull(endpoint, "endpoint");

		final String checkId = "K-" //
				+ System.currentTimeMillis() + "-" //
				+ randomAlphanumeric(20);

		final MutableCheckDto check = instantiate(MutableCheckDto.class) //
				.setId(checkId) //
				.setStartedAt(clock.now()) //
				.setStatus(CheckStatus.RUNNING);

		checksByEndpoints.put(endpoint, check);

		checksByIds.compute(checkId, (key, oldValue) -> {
			checkState(oldValue == null, "checkId: %s has already a value: %s", checkId, oldValue);
			return check;
		});

		return checkId;
	}

	@Override
	public void addCheckError( //
			final String serviceId, //
			final String endpoint, //
			@Nullable final String checkId, //
			final String errorMessage //
	) throws SQLException, IOException {

		checkNotNull(serviceId, "serviceId");
		checkNotNull(endpoint, "endpoint");
		checkNotNull(errorMessage, "errorMessage");

		if (checkId == null) {
			throw new NotImplementedException("checkId: " + checkId);
		}

		final MutableCheckDto check = checksByIds.get(checkId);

		if (check.getEndedAt() == null) {
			check.setEndedAt(clock.now());
		}

		check.setStatus(CheckStatus.ERROR);

		if (check.getErrorMessage() == null) {
			check.setErrorMessage(errorMessage);
		}
	}

	@Override
	public CheckDto getCheck( //
			final String checkId //
	) throws SQLException, IOException {

		checkNotNull(checkId, "checkId");

		return checksByIds.get(checkId);
	}

	@Override
	public void endCheck( //
			final String checkId, //
			final int elapsedMs, //
			final int statusCode //
	) throws SQLException, IOException {

		checkNotNull(checkId, "checkId");

		final MutableCheckDto check = checksByIds.get(checkId);

		synchronized (check) {

			checkState(check.getEndedAt() == null, "check.endedAt for checkId: %s should be null, but was: %s", checkId,
					check.getEndedAt());
			checkState(check.getErrorMessage() == null,
					"check.errorMessage for checkId: %s should be null, but was: %s", checkId, check.getErrorMessage());
			checkState(check.getStatus() == CheckStatus.RUNNING,
					"check.status for checkId: %s should be RUNNING, but was: %s", checkId, check.getStatus());

			check.setEndedAt(clock.now());
			check.setElapsedMs(elapsedMs);
			check.setStatusCode(statusCode);
			check.setStatus(CheckStatus.SUCCESS);
		}
	}
}
