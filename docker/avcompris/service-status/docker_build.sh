#!/bin/sh

# File: avc-service-status/docker/avcompris/service-status/docker_build.sh
#
# This script may be run manually, with the Dockerfile aside.
#

set -e

PROJECT_NAME=avc-service-status
IMAGE_NAME=avcompris/service-status

docker build -t "${IMAGE_NAME}" .

# REPO="repo.avcompris.com:5000"
#
# docker tag "${IMAGE_NAME}" "repo.avcompris.com:5000/${IMAGE_NAME}"
# docker push "repo.avcompris.com:5000/${IMAGE_NAME}"

REPO="registry.gitlab.com/avcompris/${PROJECT_NAME}"

ARTIFACT_ID="${PROJECT_NAME}-web"

VERSION=`grep \<version\> "../../../${ARTIFACT_ID}/pom.xml" | head -1 | awk -F "\>|\<" '{ print $3 }'`

docker tag "${IMAGE_NAME}" "${REPO}/${IMAGE_NAME}"
docker tag "${IMAGE_NAME}" "${REPO}/${IMAGE_NAME}:${VERSION}"

docker push                "${REPO}/${IMAGE_NAME}"
docker push                "${REPO}/${IMAGE_NAME}:${VERSION}"

echo "Done."
