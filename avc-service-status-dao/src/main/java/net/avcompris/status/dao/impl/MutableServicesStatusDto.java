package net.avcompris.status.dao.impl;

import net.avcompris.status.dao.ServiceStatusDto;
import net.avcompris.status.dao.ServicesStatusDto;

public interface MutableServicesStatusDto extends ServicesStatusDto {

	MutableServicesStatusDto addToItems(ServiceStatusDto item);
}
