package net.avcompris.status.dao;

public interface EndpointDto {

	String getServiceId();

	String getEndpoint();

	EndpointDto setServiceId(String serviceId);

	EndpointDto setEndpoint(String endpoint);
}
