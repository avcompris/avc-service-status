package net.avcompris.status.dao;

public interface ServicesStatusDto {

	ServiceStatusDto[] getItems();
}
