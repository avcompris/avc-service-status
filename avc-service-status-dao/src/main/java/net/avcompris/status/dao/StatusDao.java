package net.avcompris.status.dao;

import java.io.IOException;
import java.sql.SQLException;

import javax.annotation.Nullable;

public interface StatusDao {

	ServicesStatusHistoryDto getServicesCachedStatus(EndpointDto... endpoints) throws SQLException, IOException;

	String initCheck( //
			String serviceId, //
			String endpoint //
	) throws SQLException, IOException;

	void addCheckError( //
			String serviceId, //
			String endpoint, //
			@Nullable String checkId, //
			String errorMessage //
	) throws SQLException, IOException;

	void endCheck( //
			String checkId, //
			int elapsedMs, //
			int statusCode //
	) throws SQLException, IOException;

	CheckDto getCheck(String checkId) throws SQLException, IOException;
}
