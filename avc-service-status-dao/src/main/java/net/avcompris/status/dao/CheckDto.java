package net.avcompris.status.dao;

import javax.annotation.Nullable;

import org.joda.time.DateTime;

import net.avcompris.status.query.CheckStatus;

public interface CheckDto {

	String getId();

	DateTime getStartedAt();

	@Nullable
	DateTime getEndedAt();

	CheckStatus getStatus();

	@Nullable
	String getErrorMessage();

	@Nullable
	Integer getElapsedMs();

	@Nullable
	Integer getStatusCode();
}
