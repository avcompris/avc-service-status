package net.avcompris.status.dao.impl;

import javax.annotation.Nullable;

import org.joda.time.DateTime;

import net.avcompris.status.dao.CheckDto;
import net.avcompris.status.query.CheckStatus;

public interface MutableCheckDto extends CheckDto {

	MutableCheckDto setId(String checkId);

	MutableCheckDto setStartedAt(DateTime startedAt);

	MutableCheckDto setEndedAt(@Nullable DateTime endedAt);

	MutableCheckDto setStatus(CheckStatus status);

	MutableCheckDto setErrorMessage(@Nullable String errorMessage);

	MutableCheckDto setElapsedMs(@Nullable Integer elapsedMs);

	MutableCheckDto setStatusCode(@Nullable Integer statusCode);
}
