package net.avcompris.status.dao.impl;

import net.avcompris.status.dao.ServiceStatusHistoryDto;
import net.avcompris.status.dao.ServicesStatusHistoryDto;

public interface MutableServicesStatusHistoryDto extends ServicesStatusHistoryDto {

	MutableServicesStatusHistoryDto addToItems(ServiceStatusHistoryDto item);
}
