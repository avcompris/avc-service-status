package net.avcompris.status.dao;

public interface ServiceStatusHistoryDto {

	String getServiceId();

	String getEndpoint();

	CheckDto[] getChecks();

	int getStart();

	int getTotal();
}
