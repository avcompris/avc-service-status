package net.avcompris.status.dao;

public interface ServicesStatusHistoryDto {

	ServiceStatusHistoryDto[] getItems();
}
