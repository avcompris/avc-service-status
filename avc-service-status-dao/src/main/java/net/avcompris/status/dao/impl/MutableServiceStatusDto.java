package net.avcompris.status.dao.impl;

import net.avcompris.status.dao.CheckDto;
import net.avcompris.status.dao.ServiceStatusDto;

public interface MutableServiceStatusDto extends ServiceStatusDto {

	MutableServiceStatusDto setServiceId(String serviceId);

	MutableServiceStatusDto setEndpoint(String endpoint);

	MutableServiceStatusDto setCheck(CheckDto check);
}
