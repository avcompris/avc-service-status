package net.avcompris.status.dao;

public interface ServiceStatusDto {

	String getServiceId();

	String getEndpoint();

	CheckDto getCheck();
}
