package net.avcompris.status.dao.impl;

import net.avcompris.status.dao.CheckDto;
import net.avcompris.status.dao.ServiceStatusHistoryDto;

public interface MutableServiceStatusHistoryDto extends ServiceStatusHistoryDto {

	MutableServiceStatusHistoryDto setServiceId(String serviceId);

	MutableServiceStatusHistoryDto setEndpoint(String endpoint);

	MutableServiceStatusHistoryDto addToChecks(CheckDto check);

	MutableServiceStatusHistoryDto setStart(int start);

	MutableServiceStatusHistoryDto setTotal(int total);
}
