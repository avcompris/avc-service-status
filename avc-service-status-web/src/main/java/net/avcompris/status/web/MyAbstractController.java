package net.avcompris.status.web;

import net.avcompris.commons3.client.SessionPropagator;
import net.avcompris.commons3.core.CorrelationService;
import net.avcompris.commons3.utils.Clock;
import net.avcompris.commons3.web.AbstractController;

abstract class MyAbstractController extends AbstractController {

	protected MyAbstractController(final CorrelationService correlationService,
			final SessionPropagator sessionPropagator, final Clock clock) {

		super(correlationService, sessionPropagator, clock);
	}

	protected final boolean isSecure() {

		return Application.isSecure();
	}

	protected final boolean isHttpOnly() {

		return Application.isHttpOnly();
	}
}
