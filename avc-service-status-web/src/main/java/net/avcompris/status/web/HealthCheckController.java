package net.avcompris.status.web;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.avcompris.commons3.api.exception.ServiceException;
import net.avcompris.commons3.client.SessionPropagator;
import net.avcompris.commons3.core.CorrelationService;
import net.avcompris.commons3.databeans.DataBeans;
import net.avcompris.commons3.utils.Clock;
import net.avcompris.commons3.utils.LogFactory;
import net.avcompris.commons3.web.HealthCheck;
import net.avcompris.commons3.web.MutableHealthCheck;

@RestController
public final class HealthCheckController extends MyAbstractController {

	@SuppressWarnings("unused")
	private static final Log logger = LogFactory.getLog(HealthCheckController.class);

	@Autowired
	public HealthCheckController(final CorrelationService correlationService, //
			final SessionPropagator sessionPropagator, //
			final Clock clock) {

		super(correlationService, sessionPropagator, clock);
	}

	@RequestMapping(value = "/healthcheck", method = GET)
	public ResponseEntity<HealthCheck> getHealthCheck(final HttpServletRequest request, //
			final HttpServletResponse response //
	) throws ServiceException {

		return wrapNonAuthenticatedWithoutCorrelationId(request, () -> {

			final HealthCheck healthCheck = DataBeans.instantiate(MutableHealthCheck.class) //
					.setOk(true) //
					.setComponentName(Application.NAME);

			return ResponseEntity.status(HttpStatus.OK) //
					.body(healthCheck);
		});
	}
}
