package net.avcompris.status.web;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.status.web.Application.API;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.avcompris.commons3.api.exception.ServiceException;
import net.avcompris.commons3.client.SessionPropagator;
import net.avcompris.commons3.core.CorrelationService;
import net.avcompris.commons3.utils.Clock;
import net.avcompris.commons3.utils.LogFactory;
import net.avcompris.status.api.InlineService;
import net.avcompris.status.api.ServiceStatus;
import net.avcompris.status.api.ServiceStatusHistory;
import net.avcompris.status.api.ServicesStatus;
import net.avcompris.status.api.ServicesStatusHistory;
import net.avcompris.status.api.StatusConfig;
import net.avcompris.status.api.StatusService;

@RestController
public final class StatusController extends MyAbstractController {

	@SuppressWarnings("unused")
	private static final Log logger = LogFactory.getLog(StatusController.class);

	private final StatusService statusService;

	@Autowired
	public StatusController(final CorrelationService correlationService, final StatusService statusService, //
			final SessionPropagator sessionPropagator, //
			final Clock clock) {

		super(correlationService, sessionPropagator, clock);

		this.statusService = checkNotNull(statusService, "statusService");
	}

	@RequestMapping(value = API + "/status/live", method = GET)
	public ResponseEntity<ServicesStatus> getServicesLiveStatus(final HttpServletRequest request, //
			final HttpServletResponse response, //
			@RequestParam(name = "q", required = false) final String q, //
			@RequestParam(name = "sort", required = false) final String sort, //
			@RequestParam(name = "start", required = false) final Integer start, //
			@RequestParam(name = "limit", required = false) final Integer limit, //
			@RequestParam(name = "expand", required = false) final String expand //
	) throws ServiceException {

		return wrapNonAuthenticated(request, (correlationId) -> {

			final ServicesStatus status = statusService.getServicesLiveStatus(correlationId);

			return ResponseEntity.status(HttpStatus.OK) //
					.body(status);
		});
	}

	@RequestMapping(value = API + "/status/live/{serviceId}", method = GET)
	public ResponseEntity<ServiceStatus> getServiceLiveStatus(final HttpServletRequest request, //
			final HttpServletResponse response, //
			@PathVariable(name = "serviceId", required = true) final String serviceId //
	) throws ServiceException {

		return wrapNonAuthenticated(request, (correlationId) -> {

			final ServiceStatus status = statusService.getServiceLiveStatus(correlationId, serviceId);

			return ResponseEntity.status(HttpStatus.OK) //
					.body(status);
		});
	}

	@RequestMapping(value = API + "/status/live/{serviceId}", method = POST)
	public ResponseEntity<ServiceStatus> getInlineServiceLiveStatus(final HttpServletRequest request, //
			final HttpServletResponse response, //
			@PathVariable(name = "serviceId", required = true) final String serviceId, //
			@RequestBody(required = true) final InlineService inlineService //
	) throws ServiceException {

		return wrapNonAuthenticated(request, (correlationId) -> {

			final ServiceStatus status = statusService.getInlineServiceLiveStatus(correlationId, serviceId,
					inlineService);

			return ResponseEntity.status(HttpStatus.OK) //
					.body(status);
		});
	}

	@RequestMapping(value = API + "/status/history", method = GET)
	public ResponseEntity<ServicesStatusHistory> getServicesStatusHistory(final HttpServletRequest request, //
			final HttpServletResponse response, //
			@RequestParam(name = "q", required = false) final String q, //
			@RequestParam(name = "sort", required = false) final String sort, //
			@RequestParam(name = "start", required = false) final Integer start, //
			@RequestParam(name = "limit", required = false) final Integer limit, //
			@RequestParam(name = "expand", required = false) final String expand //
	) throws ServiceException {

		return wrapNonAuthenticated(request, (correlationId) -> {

			final ServicesStatusHistory history = statusService.getServicesStatusHistory(correlationId);

			return ResponseEntity.status(HttpStatus.OK) //
					.body(history);
		});
	}

	@RequestMapping(value = API + "/status/history/{serviceId}", method = GET)
	public ResponseEntity<ServiceStatusHistory> getServiceStatusHistory(final HttpServletRequest request, //
			final HttpServletResponse response, //
			@PathVariable(name = "serviceId", required = true) final String serviceId //
	) throws ServiceException {

		return wrapNonAuthenticated(request, (correlationId) -> {

			final ServiceStatusHistory history = statusService.getServiceStatusHistory(correlationId, serviceId);

			return ResponseEntity.status(HttpStatus.OK) //
					.body(history);
		});
	}

	@RequestMapping(value = API + "/status/config", method = GET)
	public ResponseEntity<StatusConfig> getStatusConfig(final HttpServletRequest request, //
			final HttpServletResponse response //
	) throws ServiceException {

		return wrapNonAuthenticated(request, (correlationId) -> {

			final StatusConfig config = statusService.getStatusConfig(correlationId);

			return ResponseEntity.status(HttpStatus.OK) //
					.body(config);
		});
	}
}
