package net.avcompris.status.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import net.avcompris.commons3.notifier.ErrorNotifier;
import net.avcompris.commons3.web.AbstractErrorController;

@Controller
public final class ApplicationErrorController extends AbstractErrorController {

	@Autowired
	public ApplicationErrorController(final ErrorNotifier notifier) {

		super(notifier);
	}

	@Override
	protected boolean isApplicationSecure() {

		return Application.isSecure();
	}

	@Override
	protected boolean isApplicationHttpOnly() {

		return Application.isHttpOnly();
	}
}
