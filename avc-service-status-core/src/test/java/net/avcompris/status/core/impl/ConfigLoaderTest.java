package net.avcompris.status.core.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;

import org.junit.jupiter.api.Test;

import net.avcompris.status.api.StatusConfig;

public class ConfigLoaderTest {

	@Test
	public void testConfigLoader_001() throws Exception {

		final StatusConfig config = ConfigLoader.loadConfig(new File("src/test/yaml", "config-001.yml"));

		assertEquals(3, config.getServices().length);

		final StatusConfig.ServiceConfig service0 = config.getServices()[0];
		final StatusConfig.ServiceConfig service1 = config.getServices()[1];
		final StatusConfig.ServiceConfig service2 = config.getServices()[2];

		assertEquals("jenkins-ks1", service0.getId());
		assertEquals("GET https://ks1.avcompris.com/", service0.getEndpoint());
		assertEquals(2, service0.getLabels().length);
		assertEquals("jenkins", service0.getLabels()[0]);
		assertEquals("ks1", service0.getLabels()[1]);
		assertEquals(60_000, service0.getTimeOutMs().longValue());
		assertEquals(300_000, service0.getEveryMs().longValue());
		assertEquals(200, service0.getExpect().getStatusCode().intValue());

		assertEquals("jenkins-ks11", service1.getId());

		assertEquals("jenkins-ks2e", service2.getId());
	}
}
