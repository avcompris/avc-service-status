package net.avcompris.status.core.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

import javax.annotation.Nullable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.avcompris.commons3.api.User;
import net.avcompris.commons3.api.exception.ServiceException;
import net.avcompris.commons3.core.Permissions;
import net.avcompris.commons3.core.impl.AbstractServiceImpl;
import net.avcompris.commons3.dao.CorrelationDao;
import net.avcompris.commons3.dao.exception.DuplicateEntityException;
import net.avcompris.commons3.utils.Clock;
import net.avcompris.commons3.utils.LogFactory;
import net.avcompris.status.core.MyCorrelationService;

@Service
public final class CorrelationServiceImpl extends AbstractServiceImpl implements MyCorrelationService {

	private final CorrelationDao correlationDao;

	@Autowired
	public CorrelationServiceImpl(final Permissions permissions, final Clock clock,
			final CorrelationDao correlationDao) {

		super(permissions, clock);

		this.correlationDao = checkNotNull(correlationDao, "correlationDao");
	}

	@Override
	public String getCorrelationId(@Nullable final String correlationIdParam,
			@Nullable final String correlationIdHeader) throws ServiceException {

		return wrap(() -> {

			final String correlationId;

			if (correlationIdParam != null && correlationDao.isCorrelationIdValid(correlationIdParam)) {

				correlationId = correlationIdParam;

			} else if (correlationIdHeader != null && correlationDao.isCorrelationIdValid(correlationIdHeader)) {

				correlationId = correlationIdHeader;

			} else {

				correlationId = retryDaoUntil(4_000, 80, () -> {

					final String c = "C-" //
							+ clock.nowMs() + "-" //
							+ randomAlphanumeric(20);

					try {

						correlationDao.addCorrelationId(c);

					} catch (final DuplicateEntityException e) {
						return null;
					}

					return c;

				});
			}

			return correlationId;

		});
	}

	@Override
	public void purgeOlderThanSec(final String correlationId, final User user, final int seconds)
			throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(user, "user");

		LogFactory.setCorrelationId(correlationId);

		// 0. PERMISSIONS

		permissions.assertAuthorized(correlationId, user, "seconds", seconds);

		// 1. LOGIC

		wrap(()

		-> correlationDao.purgeOlderThanSec(seconds));
	}
}
