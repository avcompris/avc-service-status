package net.avcompris.status.core.impl;

import net.avcompris.commons3.types.DateTimeHolder;
import net.avcompris.status.api.ServiceStatus;
import net.avcompris.status.api.ServicesStatus;
import net.avcompris.status.api.TriggerType;

interface MutableServicesStatus extends ServicesStatus {

	MutableServicesStatus setCheckStartedAt(DateTimeHolder checkStartedAt);

	MutableServicesStatus setCheckEndedAt(DateTimeHolder checkEndedAt);

	MutableServicesStatus addToItems(ServiceStatus item);

	MutableServicesStatus setTriggerType(TriggerType triggerType);
}
