package net.avcompris.status.core.impl;

import net.avcompris.status.api.Check;
import net.avcompris.status.api.ServiceStatusHistory;

interface MutableServiceStatusHistory extends ServiceStatusHistory {

	MutableServiceStatusHistory setServiceId(String serviceId);

	MutableServiceStatusHistory setEndpoint(String endpoint);

	MutableServiceStatusHistory addToChecks(Check check);

	MutableServiceStatusHistory setStart(int start);

	MutableServiceStatusHistory setTotal(int total);
}
