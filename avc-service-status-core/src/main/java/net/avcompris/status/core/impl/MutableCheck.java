package net.avcompris.status.core.impl;

import javax.annotation.Nullable;

import net.avcompris.commons3.types.DateTimeHolder;
import net.avcompris.status.api.Check;
import net.avcompris.status.api.TriggerType;
import net.avcompris.status.query.CheckStatus;

interface MutableCheck extends Check {

	MutableCheck setId(String id);

	MutableCheck setStartedAt(DateTimeHolder startedAt);

	MutableCheck setEndedAt(@Nullable DateTimeHolder endedAt);

	MutableCheck setElapsedMs(@Nullable Integer elapsedMs);

	MutableCheck setSuccess(boolean success);

	MutableCheck setStatus(CheckStatus status);

	MutableCheck setStatusCode(@Nullable Integer statusCode);

	MutableCheck setErrorMessage(@Nullable String errorMessage);

	MutableCheck setTriggerType(TriggerType triggerType);
}
