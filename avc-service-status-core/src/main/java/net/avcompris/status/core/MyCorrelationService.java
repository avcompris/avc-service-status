package net.avcompris.status.core;

import static net.avcompris.status.api.Permission.ANY;
import static net.avcompris.status.api.Permission.PURGE_CORRELATION_IDS;

import javax.annotation.Nullable;

import net.avcompris.commons3.api.User;
import net.avcompris.commons3.api.exception.ServiceException;
import net.avcompris.commons3.core.CorrelationService;
import net.avcompris.status.api.Permissions;

public interface MyCorrelationService extends CorrelationService {

	/**
	 * Get a non-null correlationId from the HTTP request, or create a new
	 * correlationId if needed.
	 *
	 * @param correlationIdParam  the "correlationId", if any, passed as a parameter
	 *                            in the HTTP URL’s query (overwrite
	 *                            correlationIdHeader)
	 * @param correlationIdHeader the "correlationId", if any, passed as a Header in
	 *                            the HTTP Request
	 */
	@Permissions(ANY)
	@Override
	String getCorrelationId(@Nullable String correlationIdParam, @Nullable String correlationIdHeader)
			throws ServiceException;

	@Permissions(PURGE_CORRELATION_IDS)
	@Override
	void purgeOlderThanSec(String correlationId, User user, int seconds) throws ServiceException;
}
