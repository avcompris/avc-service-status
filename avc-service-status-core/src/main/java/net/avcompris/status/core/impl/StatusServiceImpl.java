package net.avcompris.status.core.impl;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import static net.avcompris.commons3.core.DateTimeHolderImpl.toDateTimeHolder;
import static net.avcompris.commons3.core.DateTimeHolderImpl.toDateTimeHolderOrNull;
import static net.avcompris.commons3.databeans.DataBeans.instantiate;
import static net.avcompris.commons3.utils.EnvUtils.getEnvProperty;
import static org.apache.commons.lang3.StringUtils.substringAfter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import javax.annotation.Nullable;

import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.logging.Log;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Iterables;

import net.avcompris.commons3.api.exception.ServiceException;
import net.avcompris.commons3.api.exception.UnexpectedException;
import net.avcompris.commons3.core.Permissions;
import net.avcompris.commons3.core.impl.AbstractServiceImpl;
import net.avcompris.commons3.utils.Clock;
import net.avcompris.commons3.utils.LogFactory;
import net.avcompris.status.api.Check;
import net.avcompris.status.api.InlineService;
import net.avcompris.status.api.ServiceStatus;
import net.avcompris.status.api.ServiceStatusHistory;
import net.avcompris.status.api.ServicesStatus;
import net.avcompris.status.api.ServicesStatusHistory;
import net.avcompris.status.api.StatusConfig;
import net.avcompris.status.api.StatusConfig.Expect;
import net.avcompris.status.api.StatusService;
import net.avcompris.status.api.TriggerType;
import net.avcompris.status.dao.CheckDto;
import net.avcompris.status.dao.EndpointDto;
import net.avcompris.status.dao.ServiceStatusHistoryDto;
import net.avcompris.status.dao.ServicesStatusHistoryDto;
import net.avcompris.status.dao.StatusDao;
import net.avcompris.status.query.CheckStatus;

@Service
public final class StatusServiceImpl extends AbstractServiceImpl implements StatusService {

	private static final Log logger = LogFactory.getLog(StatusServiceImpl.class);

	private final StatusDao statusDao;

	private final StatusConfig config;

	@Autowired
	public StatusServiceImpl(final Permissions permissions, final Clock clock, //
			final StatusDao statusDao) throws IOException {

		super(permissions, clock);

		this.statusDao = checkNotNull(statusDao, "statusDao");

		final String configFilePath = getEnvProperty("configFile", "/etc/avcompris/service-status.yml");

		final File configFile = new File(configFilePath);

		if (!configFile.isFile()) {
			throw new FileNotFoundException("configFile should exist: " + configFile.getCanonicalPath());
		}

		config = ConfigLoader.loadConfig(configFile);
	}

	@Override
	public StatusConfig getStatusConfig(final String correlationId) throws ServiceException {

		checkNotNull(correlationId, "correlationId");

		return config;
	}

	@Override
	public ServicesStatus getServicesLiveStatus(final String correlationId) throws ServiceException {

		checkNotNull(correlationId, "correlationId");

		// LogFactory.setCorrelationId(correlationId);

		final MutableServicesStatus servicesStatus = instantiate(MutableServicesStatus.class) //
				.setCheckStartedAt(toDateTimeHolder(clock.now())) //
				.setTriggerType(TriggerType.UNKNOWN);

		final List<CheckThread> threads = newArrayList();

		for (final StatusConfig.ServiceConfig service : config.getServices()) {

			threads.add(new CheckThread( //
					correlationId, //
					service.getId(), //
					service.getEndpoint(), //
					service.getTimeOutMs().intValue(), //
					service.getExpect()));
		}

		threads.stream().forEach((thread) -> thread.start());

		threads.stream().forEach((thread) -> {

			try {

				thread.join();

			} catch (final InterruptedException e) {

				try {

					addCheckError(thread.serviceId, thread.endpoint, thread.checkId, e);

				} catch (final ServiceException e2) {

					throw new RuntimeException(e2);
				}
			}
		});

		for (final CheckThread thread : threads) {

			final String checkId = thread.checkId;

			if (checkId == null) {
				continue;
			}

			final CheckDto check = wrap(() ->

			statusDao.getCheck(checkId));

			servicesStatus.addToItems(instantiate(MutableServiceStatus.class) //
					.setServiceId(thread.serviceId) //
					.setEndpoint(thread.endpoint) //
					.setCheck(dto2Check(check)));
		}

		servicesStatus.setCheckEndedAt(toDateTimeHolder(clock.now()));

		return servicesStatus;
	}

	private StatusConfig.ServiceConfig extractServiceConfig(final String serviceId) {

		for (final StatusConfig.ServiceConfig serviceConfig : config.getServices()) {

			if (serviceId.contentEquals(serviceConfig.getId())) {
				return serviceConfig;
			}
		}

		throw new IllegalArgumentException("Unknown serviceId: " + serviceId);
	}

	@Override
	public ServiceStatus getServiceLiveStatus(final String correlationId, final String serviceId)
			throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(serviceId, "serviceId");

		final StatusConfig.ServiceConfig serviceConfig = extractServiceConfig(serviceId);

		final CheckThread thread = new CheckThread( //
				correlationId, //
				serviceId, //
				serviceConfig.getEndpoint(), //
				serviceConfig.getTimeOutMs(), //
				serviceConfig.getExpect());

		thread.run();

		final String checkId = thread.checkId;

		if (checkId == null) {
			throw new UnexpectedException("Could not acquire checkId");
		}

		final CheckDto check = wrap(() ->

		statusDao.getCheck(checkId));

		return instantiate(MutableServiceStatus.class) //
				.setServiceId(thread.serviceId) //
				.setEndpoint(thread.endpoint) //
				.setCheck(dto2Check(check));
	}

	@Override
	public ServicesStatusHistory getServicesStatusHistory(final String correlationId) throws ServiceException {

		checkNotNull(correlationId, "correlationId");

		final List<EndpointDto> endpoints = newArrayList();

		for (final StatusConfig.ServiceConfig serviceConfig : config.getServices()) {

			endpoints.add(instantiate(EndpointDto.class) //
					.setServiceId(serviceConfig.getId()) //
					.setEndpoint(serviceConfig.getEndpoint()));
		}

		final ServicesStatusHistoryDto servicesStatusHistoryDto = wrap(()

		-> statusDao.getServicesCachedStatus(Iterables.toArray(endpoints, EndpointDto.class)));

		final MutableServicesStatusHistory history = instantiate(MutableServicesStatusHistory.class);

		for (final ServiceStatusHistoryDto serviceStatusHistoryDto : servicesStatusHistoryDto.getItems()) {

			history.addToItems(dto2ServiceStatusHistory(serviceStatusHistoryDto));
		}

		return history;
	}

	private static ServiceStatusHistory dto2ServiceStatusHistory(
			final ServiceStatusHistoryDto serviceStatusHistoryDto) {

		final MutableServiceStatusHistory history = instantiate(MutableServiceStatusHistory.class) //
				.setServiceId(serviceStatusHistoryDto.getServiceId()) //
				.setEndpoint(serviceStatusHistoryDto.getEndpoint()) //
				.setStart(serviceStatusHistoryDto.getStart()) //
				.setTotal(serviceStatusHistoryDto.getTotal());

		for (final CheckDto checkDto : serviceStatusHistoryDto.getChecks()) {

			history.addToChecks(dto2Check(checkDto));
		}

		return history;
	}

	private static Check dto2Check(final CheckDto checkDto) {

		final CheckStatus status = checkDto.getStatus();

		final boolean success = status == CheckStatus.SUCCESS;

		final DateTime startedAt = checkDto.getStartedAt();
		final DateTime endedAt = checkDto.getEndedAt();

		return instantiate(MutableCheck.class) //
				.setId(checkDto.getId()) //
				.setStartedAt(toDateTimeHolder(startedAt)) //
				.setEndedAt(toDateTimeHolderOrNull(endedAt)) //
				.setElapsedMs(checkDto.getElapsedMs()) //
				.setTriggerType(TriggerType.UNKNOWN) //
				.setStatus(status) //
				.setSuccess(success) //
				.setErrorMessage(checkDto.getErrorMessage()) //
				.setStatusCode(checkDto.getStatusCode());
	}

	@Override
	public ServiceStatusHistory getServiceStatusHistory(final String correlationId, final String serviceId)
			throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(serviceId, "serviceId");

		throw new NotImplementedException("");
	}

	private String initCheck(final String serviceId, final String endpoint) throws ServiceException {

		return wrap(()

		-> statusDao.initCheck(serviceId, endpoint));
	}

	private void addCheckError(final String serviceId, final String endpoint, @Nullable final String checkId,
			final Throwable error) throws ServiceException {

		logger.warn("addCheckError(): " + endpoint, error);

		wrap(()

		-> statusDao.addCheckError(serviceId, endpoint, checkId, error.getMessage()));
	}

	private void addCheckError(final String serviceId, final String endpoint, @Nullable final String checkId,
			final String errorMessage) throws ServiceException {

		logger.warn("addCheckError(): " + endpoint + ": " + errorMessage);

		wrap(()

		-> statusDao.addCheckError(serviceId, endpoint, checkId, errorMessage));
	}

	private void endCheck( //
			final String checkId, //
			final CheckResult result //
	) throws ServiceException {

		wrap(()

		-> statusDao.endCheck(checkId, result.elapsedMs, result.statusCode));
	}

	private final class CheckThread extends Thread {

		private final String correlationId;
		private final String serviceId;
		private final String endpoint;
		private final int timeOutMs;
		private final Expect expect;

		@Nullable
		private String checkId;

		public CheckThread( //
				final String correlationId, //
				final String serviceId, //
				final String endpoint, //
				final int timeOutMs, //
				final Expect expect) {

			this.correlationId = checkNotNull(correlationId, "correlationId");
			this.serviceId = checkNotNull(serviceId, "serviceId");
			this.endpoint = checkNotNull(endpoint, "endpoint");
			this.timeOutMs = timeOutMs;
			this.expect = checkNotNull(expect, "expect");
		}

		@Override
		public void run() {

			LogFactory.setCorrelationId(correlationId);

			final CheckResult result;

			try {

				checkId = initCheck(serviceId, endpoint);

				result = doCheckEndpoint(endpoint);

			} catch (final ServiceException e) {

				try {

					addCheckError(serviceId, endpoint, checkId, e);

					return;

				} catch (final ServiceException e2) {

					throw new RuntimeException(e2);
				}

			} catch (final IOException | RuntimeException | Error e) {

				try {

					addCheckError(serviceId, endpoint, checkId, e);

					return;

				} catch (final ServiceException e2) {

					throw new RuntimeException(e2);
				}
			}

			try {

				endCheck(checkId, result);

				if (expect.getStatusCode() != null) {

					final int expectedStatusCode = expect.getStatusCode();

					if (expectedStatusCode != result.statusCode) {

						addCheckError(serviceId, endpoint, checkId,
								"Expected statusCode: " + expectedStatusCode + ", but was: " + result.statusCode);

						return;
					}
				}

			} catch (final ServiceException e) {

				throw new RuntimeException(e);
			}
		}
	}

	private static CheckResult doCheckEndpoint(final String endpoint) throws IOException {

		checkArgument(endpoint.startsWith("GET "), "Endpoint should start with \"GET \", but was: %s", endpoint);

		final String url = substringAfter(endpoint, "GET").trim();

		checkArgument(url.startsWith("https://") //
				|| url.startsWith("http://"), //
				"URL should start with \"https://\" or \"http://\", but was: %s", endpoint);

		final long startMs = System.currentTimeMillis();

		final HttpURLConnection cxn = (HttpURLConnection) new URL(url).openConnection();

		final long elapsedMs = System.currentTimeMillis() - startMs;

		final int statusCode = cxn.getResponseCode();

		cxn.disconnect();

		return new CheckResult((int) elapsedMs, statusCode);
	}

	private static class CheckResult {

		public final int elapsedMs;
		public final int statusCode;

		public CheckResult( //
				final int elapsedMs, //
				final int statusCode //
		) {

			this.elapsedMs = elapsedMs;
			this.statusCode = statusCode;
		}
	}

	@Override
	public ServiceStatus getInlineServiceLiveStatus(final String correlationId, final String serviceId,
			final InlineService inlineService) throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(serviceId, "serviceId");
		checkNotNull(inlineService, "inlineService");

		// LogFactory.setCorrelationId(correlationId);

		final Expect expect = inlineService.getExpect() != null //
				? inlineService.getExpect() //
				: instantiate(InlineService.Expect.class) //
						.setStatusCode(200);

		final CheckThread thread = new CheckThread( //
				correlationId, //
				serviceId, //
				inlineService.getEndpoint(), //
				inlineService.getTimeOutMs() != null //
						? inlineService.getTimeOutMs().intValue() //
						: 60_000, //
				expect);

		thread.run();

		final String checkId = thread.checkId;

		if (checkId == null) {
			throw new UnexpectedException("Could not acquire checkId");
		}

		final CheckDto check = wrap(() ->

		statusDao.getCheck(checkId));

		return instantiate(MutableServiceStatus.class) //
				.setServiceId(thread.serviceId) //
				.setEndpoint(thread.endpoint) //
				.setCheck(dto2Check(check));
	}
}
