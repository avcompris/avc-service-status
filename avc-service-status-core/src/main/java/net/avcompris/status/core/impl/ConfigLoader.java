package net.avcompris.status.core.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.StringUtils.substringBeforeLast;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.lang3.NotImplementedException;

import com.google.common.collect.Iterables;

import net.avcompris.commons3.yaml.Yaml;
import net.avcompris.commons3.yaml.YamlUtils;
import net.avcompris.status.api.StatusConfig;
import net.avcompris.status.api.StatusConfig.Expect;
import net.avcompris.status.api.StatusConfig.ServiceConfig;

abstract class ConfigLoader {

	public static StatusConfig loadConfig(final File yamlFile) throws IOException {

		final Yaml yaml = YamlUtils.loadYaml(yamlFile);

		return new StatusConfigImpl(yaml);
	}

	private static final class StatusConfigImpl implements StatusConfig {

		private final Map<String, ServiceConfig> services = new TreeMap<>();

		public StatusConfigImpl(final Yaml yaml) {

			checkNotNull(yaml, "yaml");

			final Yaml servicesSection = yaml.get("services");

			for (final Object key : servicesSection.keys()) {

				final String serviceId = (String) key;

				final Yaml serviceSection = servicesSection.get(key);

				final String endpoint = serviceSection.get("endpoint").asString();

				final int statusCode = serviceSection.get("expect").get("statusCode").asInt();

				final int timeOutMs = parseMs(serviceSection.get("timeOut").asString());

				final int everyMs = parseMs(serviceSection.get("every").asString());

				final Set<String> labels = new TreeSet<>();

				if (serviceSection.has("labels")) {

					if (serviceSection.get("labels").isArray()) {

						for (final Yaml label : serviceSection.get("labels").items()) {

							labels.add(label.asString());
						}

					} else {

						labels.add(serviceSection.get("labels").asString());
					}
				}

				services.put(serviceId, new ServiceImpl( //
						serviceId, //
						endpoint, //
						Iterables.toArray(labels, String.class), //
						timeOutMs, //
						everyMs, //
						new ExpectImpl(statusCode)));
			}
		}

		@Override
		public ServiceConfig[] getServices() {

			return Iterables.toArray(services.values(), ServiceConfig.class);
		}
	}

	private static int parseMs(final String s) {

		if (s.endsWith("ms")) {

			return Integer.parseInt(substringBeforeLast(s, "ms").trim());

		} else if (s.endsWith("min")) {

			return 60_000 * Integer.parseInt(substringBeforeLast(s, "min").trim());

		} else {

			throw new NotImplementedException("s: " + s);
		}
	}

	private static final class ServiceImpl implements ServiceConfig {

		private final String id;
		private final String endpoint;
		private final String[] labels;
		private final int timeOutMs;
		private final int everyMs;
		private final Expect expect;

		public ServiceImpl( //
				final String id, //
				final String endpoint, //
				final String[] labels, //
				final int timeOutMs, //
				final int everyMs, //
				final Expect expect) {

			this.id = checkNotNull(id, "id");
			this.endpoint = checkNotNull(endpoint, "endpoint");
			checkNotNull(labels, "endpoint");
			this.labels = Arrays.copyOf(labels, labels.length);
			this.timeOutMs = timeOutMs;
			this.everyMs = everyMs;
			this.expect = checkNotNull(expect, "expect");
		}

		@Override
		public String getId() {
			return id;
		}

		@Override
		public String getEndpoint() {
			return endpoint;
		}

		@Override
		public String[] getLabels() {
			return Arrays.copyOf(labels, labels.length);
		}

		@Override
		public Integer getTimeOutMs() {
			return timeOutMs;
		}

		@Override
		public Integer getEveryMs() {
			return everyMs;
		}

		@Override
		public Expect getExpect() {
			return expect;
		}
	}

	private static final class ExpectImpl implements Expect {

		private final int statusCode;

		public ExpectImpl(final int statusCode) {

			this.statusCode = statusCode;
		}

		@Override
		public Integer getStatusCode() {
			return statusCode;
		}
	}
}
