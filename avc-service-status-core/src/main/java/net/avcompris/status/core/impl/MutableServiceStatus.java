package net.avcompris.status.core.impl;

import net.avcompris.status.api.Check;
import net.avcompris.status.api.ServiceStatus;

interface MutableServiceStatus extends ServiceStatus {

	MutableServiceStatus setServiceId(String serviceId);

	MutableServiceStatus setEndpoint(String endpoint);

	MutableServiceStatus setCheck(Check check);
}
