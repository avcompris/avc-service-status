package net.avcompris.status.core.impl;

import net.avcompris.status.api.ServiceStatusHistory;
import net.avcompris.status.api.ServicesStatusHistory;

interface MutableServicesStatusHistory extends ServicesStatusHistory {

	MutableServicesStatusHistory addToItems(ServiceStatusHistory item);
}
