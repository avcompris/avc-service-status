package net.avcompris.status.dao.impl;

import static net.avcompris.commons3.dao.DbTable.Type.INTEGER;
import static net.avcompris.commons3.dao.DbTable.Type.TIMESTAMP_WITH_TIMEZONE;
import static net.avcompris.commons3.dao.DbTable.Type.VARCHAR;
import static net.avcompris.commons3.dao.DbTablesUtils.column;

import java.util.Arrays;

import net.avcompris.commons3.dao.DbTable;
import net.avcompris.commons3.dao.DbTablesUtils.Column;

public enum DbTables implements DbTable {

	CORRELATIONS( //
			column("correlation_id") //
					.type(VARCHAR) //
					.size(255) //
					.notNull() //
					.regexp("[a-zA-Z]+[a-zA-Z0-9-_]*") //
					.primaryKey() //
					.build(), //
			column("created_at") //
					.type(TIMESTAMP_WITH_TIMEZONE) //
					.notNull() //
					.build()),

	SERVICES( //
			column("endpoint") //
					.type(VARCHAR) //
					.size(255) //
					.notNull() //
					.primaryKey() //
					.build()), //

	SERVICES_CHECKS( //
			column("check_id") //
					.type(VARCHAR) //
					.size(255) //
					.notNull() //
					.primaryKey() //
					.build(), //
			column("endpoint") //
					.type(VARCHAR) //
					.size(255) //
					.notNull() //
					.refKey(SERVICES, "endpoint", true) //
					.build(), //
			column("status") //
					.type(VARCHAR) //
					.size(20) //
					.notNull() //
					.index() //
					.build(), //
			column("started_at") //
					.type(TIMESTAMP_WITH_TIMEZONE) //
					.notNull() //
					.build(), //
			column("ended_at") //
					.type(TIMESTAMP_WITH_TIMEZONE) //
					.nullable() //
					.build(), //
			column("elapsed_ms") //
					.type(INTEGER) //
					.nullable() //
					.build(), //
			column("status_code") //
					.type(INTEGER) //
					.nullable() //
					.build(), //
			column("error_message") //
					.type(VARCHAR) //
					.size(255) //
					.nullable() //
					.build()),

	SERVICES_CHECKS_ERRORS( //
			column("service_id") //
					.type(VARCHAR) //
					.size(255) //
					.notNull() //
					.index() //
					.build(), //
			column("endpoint") //
					.type(VARCHAR) //
					.size(255) //
					.notNull() //
					.refKey(SERVICES, "endpoint", true) //
					.build(), //
			column("check_id") //
					.type(VARCHAR) //
					.size(255) //
					.nullable() //
					.refKey(SERVICES_CHECKS, "check_id", true) //
					.build(), //
			column("error_at") //
					.type(TIMESTAMP_WITH_TIMEZONE) //
					.notNull() //
					.build(), //
			column("error_message") //
					.type(VARCHAR) //
					.size(255) //
					.nullable() //
					.build());

	private final Column[] columns;

	private DbTables(final Column... columns) {

		this.columns = columns;
	}

	@Override
	public Column[] columns() {

		return Arrays.copyOf(columns, columns.length);
	}
}
