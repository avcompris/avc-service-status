package net.avcompris.status.dao.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static net.avcompris.commons3.databeans.DataBeans.instantiate;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Set;

import javax.annotation.Nullable;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import net.avcompris.commons3.dao.impl.AbstractDaoInRDS;
import net.avcompris.commons3.utils.Clock;
import net.avcompris.status.dao.CheckDto;
import net.avcompris.status.dao.EndpointDto;
import net.avcompris.status.dao.ServicesStatusHistoryDto;
import net.avcompris.status.dao.StatusDao;
import net.avcompris.status.query.CheckStatus;

@Component
public final class StatusDaoInRDS extends AbstractDaoInRDS implements StatusDao {

	private final boolean debug;

	@Autowired
	public StatusDaoInRDS( //
			@Value("#{rds.dataSource}") final DataSource dataSource, //
			@Value("#{rds.tableNames.correlations}") final String tableName, //
			final Clock clock) {

		super(dataSource, tableName, clock);

		debug = System.getProperty("debug") != null;
	}

	@Override
	public ServicesStatusHistoryDto getServicesCachedStatus(final EndpointDto... endpoints)
			throws SQLException, IOException {

		checkNotNull(endpoints, "endpoints");

		final MutableServicesStatusHistoryDto history = instantiate(MutableServicesStatusHistoryDto.class);

		for (final EndpointDto e : endpoints) {

			final String serviceId = e.getServiceId();
			final String endpoint = e.getEndpoint();

			final MutableServiceStatusHistoryDto serviceStatus = instantiate(MutableServiceStatusHistoryDto.class) //
					.setServiceId(serviceId) //
					.setEndpoint(endpoint) //
					.setStart(0) //
					.setTotal(getCheckTotalCount(endpoint));

			history.addToItems(serviceStatus);

			for (final CheckDto check : getChecks(endpoint)) {

				serviceStatus.addToChecks(check);
			}
		}

		return history;
	}

	private int getCheckTotalCount(final String endpoint) throws SQLException {

		try (Connection cxn = getConnection()) {

			try (PreparedStatement pstmt = cxn.prepareStatement("SELECT" //
					+ " COUNT(1)" //
					+ " FROM " + tableName + "_checks" //
					+ " WHERE endpoint = ?" //
			)) {

				setString(pstmt, 1, endpoint);

				try (ResultSet rs = pstmt.executeQuery()) {

					rs.next();

					return getInt(rs, 1);
				}
			}
		}
	}

	private Iterable<CheckDto> getChecks(final String endpoint) throws SQLException {

		final List<CheckDto> checks = newArrayList();

		try (Connection cxn = getConnection()) {

			try (PreparedStatement pstmt = cxn.prepareStatement("SELECT" //
					+ " c.check_id," //
					+ " c.status," //
					+ " c.started_at," //
					+ " c.ended_at," //
					+ " c.elapsed_ms," //
					+ " c.status_code," //
					+ " e.error_message" //
					+ " FROM " + tableName + "_checks AS c" //
					+ " LEFT JOIN " + tableName + "_checks_errors AS e" //
					+ " ON c.check_id = e.check_id" //
					+ " WHERE c.endpoint = ?" //
					+ " ORDER BY started_at DESC" //
			)) {

				setString(pstmt, 1, endpoint);

				try (ResultSet rs = pstmt.executeQuery()) {

					final Set<String> checkIds = newHashSet();

					while (rs.next()) {

						final String checkId = getString(rs, "check_id");

						if (checkIds.contains(checkId)) {
							continue;
						}

						checks.add(instantiate(MutableCheckDto.class) //
								.setId(checkId) //
								.setStatus(getEnum(rs, "status", CheckStatus.class)) //
								.setStartedAt(getDateTime(rs, "started_at")) //
								.setEndedAt(getDateTime(rs, "ended_at")) //
								.setElapsedMs(getInteger(rs, "elapsed_ms")) //
								.setStatusCode(getInteger(rs, "status_code")) //
								.setErrorMessage(getString(rs, "error_message")));
					}
				}
			}
		}

		return checks;
	}

	@Override
	public String initCheck( //
			final String serviceId, //
			final String endpoint //
	) throws SQLException, IOException {

		checkNotNull(serviceId, "serviceId");
		checkNotNull(endpoint, "endpoint");

		final String checkId;

		try (Connection cxn = getConnection()) {

			ensureEndpoint(cxn, endpoint);

			checkId = "K-" //
					+ System.currentTimeMillis() + "-" //
					+ randomAlphanumeric(20);

			try (PreparedStatement pstmt = cxn.prepareStatement("INSERT INTO " + tableName + "_checks" //
					+ " (check_id," //
					+ " endpoint," //
					+ " status," //
					+ " started_at)" //
					+ " VALUES (?, ?, ?, ?)" //
			)) {

				setString(pstmt, 1, checkId);
				setString(pstmt, 2, endpoint);
				setString(pstmt, 3, CheckStatus.RUNNING.name());
				setDateTime(pstmt, 4, clock.now());

				pstmt.executeUpdate();
			}
		}

		return checkId;
	}

	private void ensureEndpoint( //
			final Connection cxn, //
			final String endpoint //
	) throws SQLException {

		try (PreparedStatement pstmt = cxn.prepareStatement("SELECT" //
				+ " 1" //
				+ " FROM " + tableName //
				+ " WHERE endpoint = ?" //
		)) {

			setString(pstmt, 1, endpoint);

			try (ResultSet rs = pstmt.executeQuery()) {

				if (rs.next()) {

					return; // The endpoint already exists in the DataBase
				}
			}
		}

		try (PreparedStatement pstmt = cxn.prepareStatement("INSERT INTO " + tableName //
				+ " (endpoint)" //
				+ " VALUES (?)" //
		)) {

			setString(pstmt, 1, endpoint);

			try {

				pstmt.executeUpdate();

			} catch (final SQLIntegrityConstraintViolationException e) {

				// do nothing if rare concurrent condition occurs
			}
		}
	}

	@Override
	public void addCheckError( //
			final String serviceId, //
			final String endpoint, //
			@Nullable final String checkId, //
			final String errorMessage //
	) throws SQLException, IOException {

		checkNotNull(serviceId, "serviceId");
		checkNotNull(endpoint, "endpoint");
		checkNotNull(errorMessage, "errorMessage");

		try (Connection cxn = getConnection()) {

			if (checkId != null) {

				cxn.setAutoCommit(false);

				try (PreparedStatement pstmt = cxn.prepareStatement("UPDATE " + tableName + "_checks" //
						+ " SET error_message = ?," //
						+ " ended_at = ?," //
						+ " status = ?" //
						+ " WHERE check_id = ?" //
						+ " AND error_message IS NULL" //
				)) {

					setString(pstmt, 1, errorMessage);
					setDateTime(pstmt, 2, clock.now());
					setString(pstmt, 3, CheckStatus.ERROR.name());

					setString(pstmt, 4, checkId);

					pstmt.executeUpdate();
				}
			}

			try (PreparedStatement pstmt = cxn.prepareStatement("INSERT INTO " + tableName + "_checks_errors" //
					+ " (service_id," //
					+ " endpoint," //
					+ " check_id," //
					+ " error_at," //
					+ " error_message)" //
					+ " VALUES (?, ?, ?, ?, ?)" //
			)) {

				setString(pstmt, 1, serviceId);
				setString(pstmt, 2, endpoint);
				setString(pstmt, 3, checkId);
				setDateTime(pstmt, 4, clock.now());
				setString(pstmt, 5, errorMessage);

				pstmt.executeUpdate();
			}

			if (checkId != null) {
				cxn.commit();
			}
		}
	}

	@Override
	public void endCheck( //
			final String checkId, //
			final int elapsedMs, //
			final int statusCode //
	) throws SQLException, IOException {

		checkNotNull(checkId, "checkId");

		try (Connection cxn = getConnection()) {

			try (PreparedStatement pstmt = cxn.prepareStatement("UPDATE " + tableName + "_checks" //
					+ " SET status = ?," //
					+ " ended_at = ?," //
					+ " elapsed_ms = ?," //
					+ " status_code = ?" //
					+ " WHERE check_id = ?" //
					+ " AND status = ?" //
					+ " AND ended_at IS NULL" //
					+ " AND error_message IS NULL" //
			)) {

				setString(pstmt, 1, CheckStatus.SUCCESS.name());
				setDateTime(pstmt, 2, clock.now());
				setInt(pstmt, 3, elapsedMs);
				setInt(pstmt, 4, statusCode);

				setString(pstmt, 5, checkId);
				setString(pstmt, 6, CheckStatus.RUNNING.name());

				final int updated = pstmt.executeUpdate();

				if (updated != 1) {
					throw new IllegalStateException("Cannot update checkId: " + checkId);
				}
			}
		}
	}

	@Override
	public CheckDto getCheck( //
			final String checkId //
	) throws SQLException, IOException {

		checkNotNull(checkId, "checkId");

		try (Connection cxn = getConnection()) {

			try (PreparedStatement pstmt = cxn.prepareStatement("SELECT" //
					+ " status," //
					+ " started_at," //
					+ " ended_at," //
					+ " elapsed_ms," //
					+ " status_code," //
					+ " error_message" //
					+ " FROM " + tableName + "_checks" //
					+ " WHERE check_id = ?" //
			)) {

				setString(pstmt, 1, checkId);

				try (ResultSet rs = pstmt.executeQuery()) {

					if (!rs.next()) {
						checkState(false, "checkId not found: %s", checkId);
					}

					return instantiate(MutableCheckDto.class) //
							.setId(checkId) //
							.setStatus(getEnum(rs, "status", CheckStatus.class)) //
							.setStartedAt(getDateTime(rs, "started_at")) //
							.setEndedAt(getDateTime(rs, "ended_at")) //
							.setElapsedMs(getInteger(rs, "elapsed_ms")) //
							.setStatusCode(getInteger(rs, "status_code")) //
							.setErrorMessage(getString(rs, "error_message"));
				}
			}
		}
	}
}
