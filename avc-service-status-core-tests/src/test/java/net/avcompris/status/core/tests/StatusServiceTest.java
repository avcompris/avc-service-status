package net.avcompris.status.core.tests;

import net.avcompris.commons3.utils.Clock;
import net.avcompris.status.dao.StatusDao;
import net.avcompris.status.dao.impl.StatusDaoInMemory;

public class StatusServiceTest extends AbstractStatusServiceTest {

	@Override
	protected StatusDao getBeans(final Clock clock) throws Exception {

		final StatusDao statusDao = new StatusDaoInMemory(clock);

		return statusDao;
	}
}
