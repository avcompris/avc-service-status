package net.avcompris.status.core.tests;

import static com.google.common.io.Resources.getResource;
import static com.google.common.io.Resources.toByteArray;
import static net.avcompris.commons3.core.tests.CoreTestUtils.defaultClock;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.BeforeEach;

import net.avcompris.commons3.api.tests.TestsSpec.TestSpec;
import net.avcompris.commons3.core.Permissions;
import net.avcompris.commons3.core.impl.PermissionsImpl;
import net.avcompris.commons3.core.tests.AbstractServiceApiTest;
import net.avcompris.status.api.StatusService;
import net.avcompris.status.core.impl.StatusServiceImpl;
import net.avcompris.status.dao.StatusDao;
import net.avcompris.status.web.HealthCheckController;
import net.avcompris.status.web.StatusController;

public abstract class AbstractStatusServiceApiTest extends AbstractServiceApiTest<StatusDao> {

	@SuppressWarnings("unchecked")
	protected AbstractStatusServiceApiTest(final TestSpec spec) {

		super(spec, "tutu", //
				HealthCheckController.class, //
				StatusController.class);
	}

	@BeforeEach
	public final void setUpBeans() throws Exception {

		final StatusDao statusDao = getBeans(defaultClock());

		final Permissions permissions = new PermissionsImpl();

		System.setProperty("configFile", "target/service_status.yml");

		FileUtils.writeByteArrayToFile(new File("target", "service_status.yml"),
				toByteArray(getResource("service_status.yml")));

		inject(StatusService.class, //
				new StatusServiceImpl(permissions, defaultClock(), statusDao));
	}
}
