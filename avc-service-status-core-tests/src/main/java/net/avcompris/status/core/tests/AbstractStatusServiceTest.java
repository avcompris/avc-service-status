package net.avcompris.status.core.tests;

import static com.google.common.io.Resources.getResource;
import static com.google.common.io.Resources.toByteArray;
import static net.avcompris.commons3.core.tests.CoreTestUtils.defaultClock;
import static net.avcompris.commons3.core.tests.CoreTestUtils.newCorrelationId;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import net.avcompris.commons3.core.Permissions;
import net.avcompris.commons3.core.impl.PermissionsImpl;
import net.avcompris.commons3.core.tests.AbstractServiceTest;
import net.avcompris.commons3.utils.LogFactory;
import net.avcompris.status.api.ServicesStatusHistory;
import net.avcompris.status.api.StatusService;
import net.avcompris.status.core.impl.StatusServiceImpl;
import net.avcompris.status.dao.StatusDao;

public abstract class AbstractStatusServiceTest extends AbstractServiceTest<StatusDao> {

	protected StatusService statusService;

	@BeforeEach
	public final void setUpBeans() throws Exception {

		final StatusDao statusDao = getBeans(defaultClock());

		final Permissions permissions = new PermissionsImpl();

		System.setProperty("configFile", "target/service_status.yml");

		FileUtils.writeByteArrayToFile(new File("target", "service_status.yml"),
				toByteArray(getResource("service_status.yml")));

		statusService = new StatusServiceImpl(permissions, defaultClock(), statusDao);

		LogFactory.resetCorrelationId();
	}

	@Test
	public final void testGetServicesStatusHistory() throws Exception {

		final ServicesStatusHistory history = statusService.getServicesStatusHistory(newCorrelationId());

		assertNotNull(history);

		assertEquals(1, history.getItems().length);
		assertEquals("google", history.getItems()[0].getServiceId());
		assertEquals("GET https://www.google.com/", history.getItems()[0].getEndpoint());
	}
}
